package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts() {
		return "All posts retrieved";
	}

//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPosts() {
		return "New post created";
	}

//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid) {
		return "Viewing details of post " + postid;
	}

//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid) {
		return  "The post " + postid + " has been deleted";
	}

//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Posts for " + user + " have been retrieved.";
	}


	//S10-ACTIVITY SECTION

	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid) {
		return "Viewing details of user " + userid;
	}

	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}

	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable Long userid, @RequestHeader(value = "Authorization") String user){
		String message;

		if(user == "")
			message = "Unauthorized access";
		else
			message = "The user " + userid + " has been deleted";

		return message;
	}


}
